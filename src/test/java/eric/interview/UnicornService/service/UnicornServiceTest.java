package eric.interview.UnicornService.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import eric.interview.UnicornService.database.IdentityMarkEntity;
import eric.interview.UnicornService.database.IdentityMarkRepo;
import eric.interview.UnicornService.database.UnicornEntity;
import eric.interview.UnicornService.database.UnicornRepo;
import eric.interview.UnicornService.vo.IdentityMark;
import eric.interview.UnicornService.vo.SaveResult;
import eric.interview.UnicornService.vo.Unicorn;


@ExtendWith(MockitoExtension.class)
public class UnicornServiceTest {

    @InjectMocks
    UnicornService service;

    @Mock
    UnicornRepo unicornRepo;

    @Mock
    IdentityMarkRepo markRepo;

    Unicorn unicorn;

    Unicorn unicorn2;

    IdentityMark mark;

    @BeforeEach
    public void setUp() {
        unicorn = new Unicorn();
        unicorn.setUnicornId(1L);
        unicorn.setEyeColor("green");
        unicorn.setHairColor("blue");
        unicorn.setHeight(5);
        unicorn.setHeightUnit("cm");
        unicorn.setHornColor("yellow");
        unicorn.setHornLength(13);
        unicorn.setName("Sparkles");
        unicorn.setWeight(20);
        unicorn.setWeightUnit("lbs");

        mark = new IdentityMark();
        mark.setLocation("somewhere");
        mark.setSide("left");
        unicorn.getIdentifiableMarks().add(mark);

        unicorn2 = new Unicorn();
        unicorn2.setUnicornId(2L);
        unicorn2.setEyeColor("red");
        unicorn2.setHairColor("green");
        unicorn2.setHeight(5);
        unicorn2.setHeightUnit("in");
        unicorn2.setHornColor("yellow");
        unicorn2.setHornLength(13);
        unicorn2.setName("Rainbow");
        unicorn2.setWeight(20);
        unicorn2.setWeightUnit("lbs");
    }

    @Test
    public void testGetAllUnicorns() {
        UnicornEntity unicornE1 = new UnicornEntity(unicorn);
        List<UnicornEntity> dbResults = Arrays.asList(unicornE1, new UnicornEntity(unicorn2));
        Mockito.when(unicornRepo.findAll()).thenReturn(dbResults);
        Mockito.when(markRepo.findAllByUnicornId(unicorn.getUnicornId()))
                .thenReturn(Arrays.asList(new IdentityMarkEntity(mark)));

        List<Unicorn> results = service.getAllUnicorns();

        assertEquals(2, results.size());
        assertEquals(unicorn.getName(), results.get(0).getName());
        assertEquals(unicorn2.getName(), results.get(1).getName());
        assertEquals(mark.getLocation(), results.get(0).getIdentifiableMarks().get(0).getLocation());
    }

    @Test
    public void testGetUnicornById() {
        Optional<UnicornEntity> option = Optional.of(new UnicornEntity(unicorn));
        Mockito.when(unicornRepo.findById(unicorn.getUnicornId())).thenReturn(option);
        Mockito.when(markRepo.findAllByUnicornId(unicorn.getUnicornId()))
                .thenReturn(Arrays.asList(new IdentityMarkEntity(mark)));

        Unicorn result = service.getUnicornById(unicorn.getUnicornId());

        assertEquals(unicorn.getName(), result.getName());
        assertEquals(mark.getLocation(), result.getIdentifiableMarks().get(0).getLocation());
    }
        
    @Test
    public void testSaveUnicorn() {
        Mockito.when(unicornRepo.save(Mockito.any(UnicornEntity.class))).thenReturn(new UnicornEntity(unicorn));
        Mockito.when(markRepo.save(Mockito.any(IdentityMarkEntity.class))).thenReturn(null);

        SaveResult result = service.saveUnicorn(unicorn);

        assertEquals(1L, result.getUnicornId());
        verify(markRepo, atLeast(1)).save(Mockito.any(IdentityMarkEntity.class));
    }

    @Test
    public void testUpdateUnicornValid() {
        Optional<UnicornEntity> option = Optional.of(new UnicornEntity(unicorn));
        Mockito.when(unicornRepo.findById(Mockito.anyLong())).thenReturn(option);
        Mockito.doNothing().when(markRepo).deleteByUnicornId(Mockito.anyLong());

        Mockito.when(unicornRepo.save(Mockito.any(UnicornEntity.class))).thenReturn(new UnicornEntity(unicorn));
        Mockito.when(markRepo.save(Mockito.any(IdentityMarkEntity.class))).thenReturn(new IdentityMarkEntity(mark));

        SaveResult result = service.updateUnicorn(unicorn);

        assertEquals(1L, result.getUnicornId());
        verify(markRepo, atLeast(1)).save(Mockito.any(IdentityMarkEntity.class));

        verify(markRepo, atLeast(1)).deleteByUnicornId(Mockito.anyLong());
        verify(unicornRepo, atLeast(1)).findById(Mockito.anyLong());
    }


    @Test
    public void testUpdateUnicornWhereNotExists() {
        NoSuchElementException ex = new NoSuchElementException();
        Mockito.when(unicornRepo.findById(Mockito.anyLong())).thenThrow(ex);

        SaveResult result = service.updateUnicorn(unicorn);

        assertEquals(-1L, result.getUnicornId());
    }
}
