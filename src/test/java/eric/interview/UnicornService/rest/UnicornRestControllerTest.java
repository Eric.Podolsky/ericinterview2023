package eric.interview.UnicornService.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import eric.interview.UnicornService.service.UnicornService;
import eric.interview.UnicornService.vo.SaveResult;
import eric.interview.UnicornService.vo.Unicorn;

@ExtendWith(MockitoExtension.class)
public class UnicornRestControllerTest {
    
    @InjectMocks
    UnicornRestController controller;

    @Mock
    UnicornService unicornService;

    Unicorn unicorn;

    @BeforeEach
    public void setUp() {
        unicorn = new Unicorn();
        unicorn.setEyeColor("green");
        unicorn.setHairColor("blue");
        unicorn.setHeight(5);
        unicorn.setHeightUnit("cm");
        unicorn.setHornColor("yellow");
        unicorn.setHornLength(13);
        unicorn.setName("Sparkles");
        unicorn.setWeight(20);
        unicorn.setWeightUnit("lbs");
    }

    @Test
    public void testValidSaveRequest() {
        SaveResult response = new SaveResult(unicorn.getUnicornId());
        Mockito.when(unicornService.saveUnicorn(ArgumentMatchers.any(Unicorn.class))).thenReturn(response);
        
        SaveResult actual = controller.saveUnicorn(unicorn);
        
        assertEquals(unicorn.getUnicornId(), actual.getUnicornId());
    }

    @Test
    public void testInvalidSaveRequest() {
        unicorn.setEyeColor(null);

        SaveResult actual = controller.saveUnicorn(unicorn);
        
        assertEquals(-1L, actual.getUnicornId());
    }
}
