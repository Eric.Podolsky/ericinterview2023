package eric.interview.UnicornService.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public interface UnicornRepo extends JpaRepository<UnicornEntity, Long> {

}
