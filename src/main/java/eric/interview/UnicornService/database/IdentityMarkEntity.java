package eric.interview.UnicornService.database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import eric.interview.UnicornService.vo.IdentityMark;
import lombok.Data;

/*
 * The JPA representation of the IdentityMark Table
 */
@Entity
@Data
public class IdentityMarkEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long markId;
    Long unicornId;
    String side;
    String location;
    String mark;
    String color;

    public IdentityMarkEntity() {

    }

    public IdentityMarkEntity(IdentityMark mark) {
        this.side = mark.getSide();
        this.location = mark.getLocation();
        this.mark = mark.getMark();
        this.color = mark.getColor();
    }
}

