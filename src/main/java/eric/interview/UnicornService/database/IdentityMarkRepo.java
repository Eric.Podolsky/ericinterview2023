package eric.interview.UnicornService.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Component
@Repository
public interface IdentityMarkRepo extends JpaRepository<IdentityMarkEntity, Long> {

    @Query(value = "SELECT * FROM IDENTITY_MARK_ENTITY WHERE UNICORN_ID = ?", nativeQuery = true)
    public List<IdentityMarkEntity> findAllByUnicornId(long unicornId);
    
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM IDENTITY_MARK_ENTITY WHERE UNICORN_ID = ?", nativeQuery = true)
    public void deleteByUnicornId(long unicornId);

}