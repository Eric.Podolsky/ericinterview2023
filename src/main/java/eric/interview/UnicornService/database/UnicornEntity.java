package eric.interview.UnicornService.database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import eric.interview.UnicornService.vo.Unicorn;
import lombok.Data;

/*
 * The JPA representation of the Unicorn Table
 */
@Entity
@Data
public class UnicornEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long unicornId;
    String name;
    String hairColor;
    // I think they forgot about hornLength units. Would ask if this was real
    double hornLength;
    String hornColor;
    String eyeColor;
    double height;
    String heightUnit;
    double weight;
    String weightUnit;

    public UnicornEntity() {

    }

    public UnicornEntity(Unicorn unicorn) {
        this.unicornId = unicorn.getUnicornId();
        this.name = unicorn.getName();
        this.hairColor = unicorn.getHairColor();
        this.hornLength = unicorn.getHornLength();
        this.hornColor = unicorn.getHornColor();
        this.eyeColor = unicorn.getEyeColor();
        this.height = unicorn.getHeight();
        this.heightUnit = unicorn.getHeightUnit();
        this.weight = unicorn.getWeight();
        this.weightUnit = unicorn.getWeightUnit();
    }
}
