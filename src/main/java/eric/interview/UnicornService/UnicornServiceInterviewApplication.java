package eric.interview.UnicornService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("eric.interview.UnicornService.database")
public class UnicornServiceInterviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnicornServiceInterviewApplication.class, args);
	}

}
