package eric.interview.UnicornService.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eric.interview.UnicornService.database.IdentityMarkEntity;
import eric.interview.UnicornService.database.IdentityMarkRepo;
import eric.interview.UnicornService.database.UnicornEntity;
import eric.interview.UnicornService.database.UnicornRepo;
import eric.interview.UnicornService.vo.IdentityMark;
import eric.interview.UnicornService.vo.SaveResult;
import eric.interview.UnicornService.vo.Unicorn;

@Component
public class UnicornService {

    @Autowired
    private UnicornRepo unicornRepo;

    @Autowired
    private IdentityMarkRepo markRepo;

    public List<Unicorn> getAllUnicorns() {
        List<UnicornEntity> repoResults = unicornRepo.findAll();

        List<Unicorn> unicorns = repoResults.stream()
                .map(entity -> new Unicorn(entity))
                .collect(Collectors.toList());

        unicorns.forEach(unicorn -> addMarks(unicorn));

        return unicorns;
    }

    public Unicorn getUnicornById(Long id) {

        try {
            Unicorn unicorn = new Unicorn(unicornRepo.findById(id).get());
            addMarks(unicorn);
            return unicorn;

        } catch (NoSuchElementException e) {
            return new Unicorn();
        }

    }

    private void addMarks(Unicorn unicorn) {
        List<IdentityMarkEntity> marks = markRepo.findAllByUnicornId(unicorn.getUnicornId());
        if (marks != null) {
            marks.forEach(mark -> unicorn.getIdentifiableMarks().add(new IdentityMark(mark)));
        }

    }


    public SaveResult saveUnicorn(Unicorn unicorn) {
        UnicornEntity savedUnicorn = unicornRepo.save(new UnicornEntity(unicorn));

        unicorn.getIdentifiableMarks().forEach(mark -> {
            IdentityMarkEntity entity = new IdentityMarkEntity(mark);
            entity.setUnicornId(savedUnicorn.getUnicornId());
            markRepo.save(entity);
        });

        return new SaveResult(savedUnicorn.getUnicornId());

    }

    public SaveResult updateUnicorn(Unicorn unicorn) {
        try {
            unicornRepo.findById(unicorn.getUnicornId()).get();
        }
        catch(NoSuchElementException e) {
            return new SaveResult(-1L);
        }

        // Since we can't be sure which Marks to edit, need to delete all of them and save fresh
        markRepo.deleteByUnicornId(unicorn.getUnicornId());

        return saveUnicorn(unicorn);

    }
}
