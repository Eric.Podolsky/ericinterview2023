package eric.interview.UnicornService.vo;

import lombok.Data;

@Data
public class SaveResult {

    Long unicornId;

    public SaveResult() {

    }

    public SaveResult(Long num) {
        this.unicornId = num;
    }
}
