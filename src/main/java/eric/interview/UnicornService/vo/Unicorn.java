package eric.interview.UnicornService.vo;

import java.util.ArrayList;
import java.util.List;

import eric.interview.UnicornService.database.UnicornEntity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/*
 * The Request/Response version of Unicorn
 */
@Data
public class Unicorn {

    Long unicornId;
    String name;
    String hairColor;
    // I think they forgot about hornLength units. Would ask if this was real
    double hornLength;
    String hornColor;
    String eyeColor;
    double height;
    String heightUnit;
    double weight;
    String weightUnit;

    @Setter(AccessLevel.NONE)
    List<IdentityMark> identifiableMarks = new ArrayList<IdentityMark>();

    public Unicorn() {

    }

    public Unicorn(UnicornEntity entity) {
        this.unicornId = entity.getUnicornId();
        this.name = entity.getName();
        this.hairColor = entity.getHairColor();
        this.hornLength = entity.getHornLength();
        this.hornColor = entity.getHornColor();
        this.eyeColor = entity.getEyeColor();
        this.height = entity.getHeight();
        this.heightUnit = entity.getHeightUnit();
        this.weight = entity.getWeight();
        this.weightUnit = entity.getWeightUnit();
    }
}