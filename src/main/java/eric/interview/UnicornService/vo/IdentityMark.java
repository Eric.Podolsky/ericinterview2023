package eric.interview.UnicornService.vo;

import eric.interview.UnicornService.database.IdentityMarkEntity;
import lombok.Data;

/*
 * The Request/Response version of IdentityMark
 */
@Data
public class IdentityMark {

    String side;
    String location;
    String mark;
    String color;

    public IdentityMark() {

    }
    
    public IdentityMark(IdentityMarkEntity entity) {
        this.side = entity.getSide();
        this.location = entity.getLocation();
        this.mark = entity.getMark();
        this.color = entity.getColor();
    }
}
