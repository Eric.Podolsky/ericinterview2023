package eric.interview.UnicornService.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import eric.interview.UnicornService.service.UnicornService;
import eric.interview.UnicornService.vo.SaveResult;
import eric.interview.UnicornService.vo.Unicorn;

@RestController
public class UnicornRestController {

    @Autowired
    private UnicornService unicornService;

    @GetMapping("/unicorns")
    public List<Unicorn> getAllUnicorns() {

        return unicornService.getAllUnicorns();
    }
    
    @GetMapping("/unicorns/{unicornId}")
    public Unicorn getUnicornById(@PathVariable Long unicornId) {
        return unicornService.getUnicornById(unicornId);
    }

    @PostMapping("/unicorns")
    public SaveResult saveUnicorn(@RequestBody Unicorn unicorn) {

        if (!isValidUnicornRequest(unicorn)) {
            return new SaveResult(-1L);
        }

        return unicornService.saveUnicorn(unicorn);
    }

    @PutMapping("/unicorns/{unicornId}")
    public SaveResult updateUnicorn(@PathVariable Long unicornId, @RequestBody Unicorn unicorn) {

        /*
         * In the example sent to me, there's a UnicornId in the path, as well as in the request body. I assumed that
         * the one in the body wouldn't actually be there
         * 
         * If it is supposed to be there, I would validate that the 2 ids match, or send a 400 back if not
         */
        if (!isValidUnicornRequest(unicorn)) {
            return new SaveResult(-1L);
        }

        unicorn.setUnicornId(unicornId);
        return unicornService.updateUnicorn(unicorn);

    }

    private boolean isValidUnicornRequest(Unicorn unicorn) {
        if (isBlank(unicorn.getName())) {
            return false;
        }
        if (isBlank(unicorn.getHairColor())) {
            return false;
        }
        if (unicorn.getHornLength() == 0) { // Maybe can be 0 if no horn? But then that's a horse, so will assume not
            return false;
        }
        if (isBlank(unicorn.getHornColor())) {
            return false;
        }
        if (isBlank(unicorn.getEyeColor())) {
            return false;
        }
        if (unicorn.getHeight() == 0) {
            return false;
        }
        if (isBlank(unicorn.getHeightUnit())) {
            return false;
        }
        if (unicorn.getWeight() == 0) {
            return false;
        }
        if (isBlank(unicorn.getWeightUnit())) {
            return false;
        }
        // Not sure if IdentifiableMarks can be empty...will assume so for now

        return true;
    }

    private boolean isBlank(String str) {
        return str == null || str.isEmpty();
    }

}
